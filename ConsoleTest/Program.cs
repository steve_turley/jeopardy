﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleTest
{
    class Program
    {
        static int timeLeft = 30;
        static AutoResetEvent autoEvent = new AutoResetEvent(false);

        static void Main(string[] args)
        {
            String path = @"..\..\..\..\Jeopardy Theme.wav";
            SoundPlayer player = new SoundPlayer(path);
            System.Threading.Timer timer = new System.Threading.Timer(callback, null, 0, 1000);
            player.Play();
            autoEvent.WaitOne(40000);
            Console.WriteLine("done");
        }

        static void callback(object state)
        {
            Console.WriteLine(timeLeft);
            timeLeft--;
            if (timeLeft == 0)
            {
                autoEvent.Set();
            }
        }
    }
}
