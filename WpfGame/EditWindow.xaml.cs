﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;
using log4net;

namespace Jeopardy
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EditWindow));
        game myGame;
        List<String> categoryList;
        int value = 100;
        string lastBank = "";
        int lastCategory = 0;
        int lastValue = 100;

        public EditWindow()
        {
            InitializeComponent();
            string firstBank = fillBanks();
            lastBank = firstBank;
            readFile(firstBank);
            fillDisplay(lastCategory);
            fillCorrect(lastCategory);
            fillCategories();  // must come after fillBanks, fillDisplay, and fillCorrect
        }

        private void fillDisplay(int catIndex){
            var selQuest =
                from quest in myGame.categories[catIndex].questions
                where quest.value == value.ToString()
                select quest;
            displayBox.Text = selQuest.First().display;
        }

        private void fillCorrect(int catIndex){
            var selQuest =
                from quest in myGame.categories[catIndex].questions
                where quest.value == value.ToString()
                select quest;
            correctBox.Text = selQuest.First().correct;
        }

        /// <summary>
        /// Read available question banks form local app data directory and fill combo box
        /// </summary>
        /// <returns>name of first question bank in directory</returns>
        private String fillBanks()
        {
            String folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            String path = System.IO.Path.Combine(folder, "Jeopardy");
            List<String> bankList = new List<string>();
            foreach (String filename in System.IO.Directory.EnumerateFiles(path, "*.xml"))
            {
                bankList.Add(System.IO.Path.GetFileName(filename));
                log.InfoFormat("Added {0} to bank ComboBox", System.IO.Path.GetFileName(filename));
            }
            bankBox.DataContext = bankList;
            bankBox.SelectedIndex = 0;
            return bankList.First();
        }

        /// <summary>
        /// Fill the category combo box with categories from question bank
        /// </summary>
        private void fillCategories()
        {
            categoryList = new List<string>();
            foreach(category cat in myGame.categories){
                categoryList.Add(cat.name);
            }
            categoryBox.DataContext = categoryList;
            categoryBox.Items.Refresh();
            lastCategory = 0;
            categoryBox.SelectedIndex = 0;  // This triggers a selected index changed event
        }

        /// <summary>
        /// Read a question file from the disk.  It assumes it is in the default
        /// directory for the program unless a path is given.
        /// </summary>
        /// <param name="fileName">file path</param>
        private void readFile(String fileName)
        {
            string appData = System.Environment.GetEnvironmentVariable("LOCALAPPDATA");
            // Try doing file operations on it.
            string dataDirectory = System.IO.Path.Combine(appData, "Jeopardy");
            string path = System.IO.Path.Combine(dataDirectory, fileName);
            log.InfoFormat("reading file {0}", path);
            // deserialize XML file
            using (StreamReader reader = new StreamReader(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(game));
                myGame = (game)serializer.Deserialize(reader);
                reader.Close();
            }
            // Make sure the format is okay
            verifyFormat();
        }

        private void valueClicked(object sender, RoutedEventArgs e)
        {
            RadioButton button = sender as RadioButton;
            value = int.Parse(button.Name.Substring(1));
            updateQuestion();
            lastValue = value;
        }

        /// <summary>
        /// Save current text in case it has changed
        /// </summary>
        private void saveText()
        {
            // Save current text in case it changed
            var oldQ =
                from quest in myGame.categories[lastCategory].questions
                where quest.value == lastValue.ToString()
                select quest;
            oldQ.First().display = displayBox.Text;
            oldQ.First().correct = displayBox.Text;
        }

        private void updateQuestion(){
            // Save current text in case it changed
            saveText();
            // find category
            string cat = categoryBox.SelectedValue as string;
            if (cat != null)
            {
                // Display selected text
                var myQuestions =
                    from myCat in myGame.categories
                    where myCat.name == cat
                    select myCat.questions;
                var myQuestion =
                    from quest in myQuestions.First()
                    where quest.value == value.ToString()
                    select quest;
                displayBox.Text = myQuestion.First().display;
                correctBox.Text = myQuestion.First().correct;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Save the current changes
            saveText();
            saveGame();
        }

        /// <summary>
        /// Perform basic integrity checks on the file format.
        /// </summary>
        private void verifyFormat()
        {
            // Check number of categories
            log.Info("verying question file format");
            if (myGame.categories.Count != 6)
            {
                log.ErrorFormat("Wrong category count: {0}", myGame.categories.Count);
                MessageBox.Show("Wrong category count");
                return;
            }
            foreach (category cat in myGame.categories)
            {
                // check number of questions
                if (cat.questions.Count != 5)
                {
                    log.ErrorFormat("Wrong question count {0} for category {1}",
                        cat.questions.Count, cat.name);
                    MessageBox.Show("Wrong question count for category " + cat.name);
                    return;
                }
                // Make sure there is a question value to match each row
                HashSet<String> vals = new HashSet<string>();
                foreach (question quest in cat.questions)
                {
                    vals.Add(quest.value);
                    if (quest.correct == null)
                    {
                        log.ErrorFormat("No correct answer for question with value {0}", quest.value);
                        MessageBox.Show("No correct answer for question with value " + quest.value);
                        return;
                    }
                    if (quest.display == null)
                    {
                        log.ErrorFormat("No displayed question for question with value {0}", quest.value);
                        MessageBox.Show("No displayed question for question with value " + quest.value);
                    }
                }
                String[] testValues = new string[]{"100","200","300","400","500"};
                foreach (String tVal in testValues)
                {
                    if (!vals.Contains(tVal))
                    {
                        log.ErrorFormat("No question with value {0} in category {1}", tVal, cat.name);
                        MessageBox.Show("No question for value " + tVal + " in category " + cat.name);
                        return;
                    }
                }
            }
        }

        private void categoryBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            updateQuestion();
            lastCategory = categoryBox.SelectedIndex;
        }

        private void bankBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            saveText();
            saveGame();
            string filename = bankBox.SelectedValue as String;
            readFile(filename);
            lastCategory = 0;
            fillDisplay(0);
            fillCorrect(0);
            fillCategories();
            updateQuestion();
        }

        private void saveGame(){
            if (myGame != null)
            {
                string appData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                // Try doing file operations on it.
                string dataDirectory = System.IO.Path.Combine(appData, "Jeopardy");
                string filename = bankBox.SelectedValue as String;
                string path = System.IO.Path.Combine(dataDirectory, lastBank);
                lastBank = filename; // Save selection for next saveGame
                log.InfoFormat("saving file {0}", path);
                // deserialize XML file
                using (StreamWriter writer = new StreamWriter(path))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(game));
                    serializer.Serialize(writer, myGame);
                    writer.Close();
                }
            }
        }

        private void editCategoriesButton_Click(object sender, RoutedEventArgs e)
        {
            EditCategoriesWindows win = new EditCategoriesWindows(categoryList);
            if (win.ShowDialog() == true)
            {
                categoryList = win.CategoryNames;
                for (int i = 0; i < 6; i++)
                    myGame.categories[i].name = categoryList[i];
                categoryBox.Items.Refresh();
                categoryBox.SelectedIndex = 0;
            }
        }
    }
}
