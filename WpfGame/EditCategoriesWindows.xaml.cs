﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Jeopardy
{
    /// <summary>
    /// Interaction logic for EditCategoriesWindows.xaml
    /// </summary>
    public partial class EditCategoriesWindows : Window
    {
        public List<String> CategoryNames { get; set; }

        public EditCategoriesWindows(List<String> categories)
        {
            InitializeComponent();
            c0.Text = categories[0];
            c1.Text = categories[1];
            c2.Text = categories[2];
            c3.Text = categories[3];
            c4.Text = categories[4];
            c5.Text = categories[5];
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            CategoryNames = new List<string>();
            CategoryNames.Add(c0.Text);
            CategoryNames.Add(c1.Text);
            CategoryNames.Add(c2.Text);
            CategoryNames.Add(c3.Text);
            CategoryNames.Add(c4.Text);
            CategoryNames.Add(c5.Text);
            DialogResult = true;
        }
    }
}
