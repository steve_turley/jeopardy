﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using log4net;

namespace Jeopardy
{
    /// <summary>
    /// Interaction logic for SetupWindow.xaml
    /// </summary>
    public partial class SetupWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SetupWindow));

        public SetupWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Show help text from web site
        /// </summary>
        /// <param name="sender">(ignored)</param>
        /// <param name="e">(ignored)</param>
        private void HelpButton_Click(object sender, RoutedEventArgs e)
        {
            HelpWindow win = new HelpWindow();
            win.Show();
        }

        /// <summary>
        /// Display program characteristics
        /// </summary>
        /// <param name="sender">(ignored)</param>
        /// <param name="e">(ignored)</param>
        private void aboutButton_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow win = new AboutWindow();
            win.Show();
        }

        /// <summary>
        /// Change the number of teams
        /// </summary>
        /// <param name="sender">(ignored)</param>
        /// <param name="e">(ignored)</param>
        private void numberButton_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Changing number of teams");
            MessageBox.Show("Changing the number of teams is not yet implemented");
        }

        /// <summary>
        /// Select which question banks to display
        /// </summary>
        /// <param name="sender">(ignored)</param>
        /// <param name="e">(ignored)</param>
        private void selectBankButton_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Selecting question banks");
            SelectWindow win = new SelectWindow();
            win.ShowDialog();
        }

        /// <summary>
        /// Edit questions in a bank
        /// </summary>
        /// <param name="sender">(ignored)</param>
        /// <param name="e">(ignored)</param>
        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Editing questions");
            EditWindow win = new EditWindow();
            win.ShowDialog();
        }

        /// <summary>
        /// Import a bank of questions
        /// </summary>
        /// <param name="sender">(ignored)</param>
        /// <param name="e">(ignored)</param>
        private void addBankButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.AddExtension = true;
            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;
            dlg.DefaultExt = "xml";
            dlg.Filter = "xml files|*.xml|all files|*.*";
            dlg.Title = "Question Bank File to Add";
            dlg.ValidateNames = true;
            if (dlg.ShowDialog() == true)
            {
                String fileName = System.IO.Path.GetFileName(dlg.FileName);
                log.InfoFormat("Adding file {0}", fileName);
                // Copy this file to the local AppData directory
                string appData = System.Environment.GetEnvironmentVariable("LOCALAPPDATA");
                // Try doing file operations on it.
                string newPath = System.IO.Path.Combine(appData, "Jeopardy");
                String destinationName = System.IO.Path.Combine(newPath, fileName);
                if (File.Exists(destinationName))
                {
                    log.InfoFormat("File {0} already exists, checking for replacement", destinationName);
                    // confirm replacement
                    if (MessageBox.Show("File " + fileName + " already exists.  Do you want to " +
                        "replace it?", "File Exists", MessageBoxButton.YesNo, MessageBoxImage.Question)
                        == MessageBoxResult.Yes)
                    {
                        log.Info("replacement confirmed, copying file");
                        File.Copy(dlg.FileName, destinationName);
                    }
                    else
                    {
                        log.Info("user chose not to replace file");
                    }
                }
                else
                {
                    log.InfoFormat("file doesn't exist yet, copying file {0}", destinationName);
                    File.Copy(dlg.FileName, destinationName);
                }
            }
            else
            {
                log.Info("Cancelled adding file");
            }
        }

        /// <summary>
        /// export a question bank form the local data directory to file system location
        /// of user's choice
        /// </summary>
        /// <param name="sender">(unused)</param>
        /// <param name="e">(unused)</param>
        private void exportButton_Click(object sender, RoutedEventArgs e)
        {
            log.Info("exporting file");
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.AddExtension = true;
            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;
            dlg.DefaultExt = "xml";
            dlg.Filter = "xml files|*.xml|all files|*.*";
            dlg.Title = "Question Bank File to Export";
            dlg.ValidateNames = true;
            string appData = System.Environment.GetEnvironmentVariable("LOCALAPPDATA");
            // Try doing file operations on it.
            string newPath = System.IO.Path.Combine(appData, "Jeopardy");
            dlg.InitialDirectory = newPath;
            if (dlg.ShowDialog() == true)
            {
                String fileName = System.IO.Path.GetFileName(dlg.FileName);
                log.InfoFormat("Exporting file{0}", fileName);
                string myDocumentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                SaveFileDialog sdlg = new SaveFileDialog();
                sdlg.FileName = fileName;
                sdlg.InitialDirectory = myDocumentsPath;
                sdlg.OverwritePrompt = true;
                sdlg.Title = "Exported File";
                sdlg.ValidateNames = true;
                if (sdlg.ShowDialog() == true)
                {
                    log.InfoFormat("Copying file to {0}", sdlg.FileName);
                    File.Copy(dlg.FileName, sdlg.FileName, true); // force overwrite
                }
                else
                {
                    log.Info("cancelled file export");
                }
            }
        }
    }
}
