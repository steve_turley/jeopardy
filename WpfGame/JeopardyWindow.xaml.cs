﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Deployment.Application;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Serialization;
using log4net;

namespace Jeopardy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MainWindow));

        SoundPlayer player = new SoundPlayer("Short Jeopardy Theme.wav");
        int timeLeft = 30;
        List<Button> questionList = new List<Button>();
        Brush highlightBrush = null;
        int team = 1;
        int[] scores = new int[4];
        game myGame;
        DispatcherTimer timer;
        StringCollection questionBanks;

        public MainWindow()
        {
            InitializeComponent();
            highlightBrush = stack1.Background;
            createButtonList();
            questionBanks = Properties.Settings.Default.QuestionBanks;
            if (questionBanks == null || questionBanks.Count==0)
            {
                log.Info("adding default question banks");
                questionBanks = new StringCollection() { "bank1.xml", "bank2.xml" };
                Properties.Settings.Default.QuestionBanks = questionBanks;
            }
            fileButton.DataContext = questionBanks;
            fileButton.SelectionChanged += fileButton_Click;
            log.Info("Reading questionBanks[0]");
            readFile(questionBanks[0]);
        }

        void createButtonList()
        {
            questionList.Add(r100c1);
            questionList.Add(r100c2);
            questionList.Add(r100c3);
            questionList.Add(r100c4);
            questionList.Add(r100c5);
            questionList.Add(r100c6);
            questionList.Add(r200c1);
            questionList.Add(r200c2);
            questionList.Add(r200c3);
            questionList.Add(r200c4);
            questionList.Add(r200c5);
            questionList.Add(r200c6);
            questionList.Add(r300c1);
            questionList.Add(r300c2);
            questionList.Add(r300c3);
            questionList.Add(r300c4);
            questionList.Add(r300c5);
            questionList.Add(r300c6);
            questionList.Add(r400c1);
            questionList.Add(r400c2);
            questionList.Add(r400c3);
            questionList.Add(r400c4);
            questionList.Add(r400c5);
            questionList.Add(r400c6);
            questionList.Add(r500c1);
            questionList.Add(r500c2);
            questionList.Add(r500c3);
            questionList.Add(r500c4);
            questionList.Add(r500c5);
            questionList.Add(r500c6);
        }

        /// <summary>
        /// Called when any question button is clicked
        /// </summary>
        /// <param name="sender">button which was clicked</param>
        /// <param name="e">(unused)</param>
        private void question_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            log.InfoFormat("button {0} clicked", button.Name);
            button.IsEnabled = false;
            String name = button.Name;
            startClock();
            displayQuestion(name);
            // disableQuestions();
            nextTeam();
            highlightTeam();
        }

        /// <summary>
        /// Move to the next team, wrapping back to 1 after 4
        /// </summary>
        void nextTeam()
        {
            team++;
            if (team > 4)
                team = 1;
        }

        /// <summary>
        /// Highlight the team whose turn it is
        /// </summary>
        void highlightTeam()
        {
            log.InfoFormat("highlighting team {0}", team);
            // Turn off all highlighting
            stack1.Background = null;
            stack2.Background = null;
            stack3.Background = null;
            stack4.Background = null;
            // Turn on highlighting for the selected team
            switch (team)
            {
                case 1:
                    stack1.Background = highlightBrush;
                    break;
                case 2:
                    stack2.Background = highlightBrush;
                    break;
                case 3:
                    stack3.Background = highlightBrush;
                    break;
                case 4:
                    stack4.Background = highlightBrush;
                    break;
            }
        }

        /// <summary>
        /// Reenable all questions
        /// </summary>
        void enableQuestions()
        {
            log.Info("enabling all questions");
            foreach (Button button in questionList)
            {
                button.IsEnabled = true;
            }
        }

        /// <summary>
        /// Start timing how much time they have spent on the question
        /// </summary>
        void startClock()
        {
            timer = new DispatcherTimer();  // Get new timer to make sure we kill all tick delegates
            timer.Tick += timer_Tick;
            timer.Interval = new TimeSpan(0, 0, 0, 1);
            timer.Start();
            timeLeft = 30;
            timeLabel.Content = "30";
            player.Play(); // plays Jeopardy theme song
        }

        /// <summary>
        /// called every second while a question is being answered
        /// </summary>
        /// <param name="sender">(unused)</param>
        /// <param name="e">(unused)</param>
        void timer_Tick(object sender, EventArgs e)
        {
            timeLeft--; // decrement number of seconds left
            timeLabel.Content = timeLeft.ToString().PadLeft(2); // pad string for better display
            if (timeLeft == 0)
            {
                // No time left, stop music and timer
                player.Stop();
                DispatcherTimer timer = sender as DispatcherTimer;
                timer.Stop();
            }
        }

        /// <summary>
        /// Stop clock synchronously
        /// </summary>
        void stopClock()
        {
            player.Stop(); // stop music
            timer.Stop(); // stop timer
            timeLeft = 30;
            timeLabel.Content = "";
        }

        /// <summary>
        /// Display the chosen question.  If answered correctly, allocated points
        /// to the team.
        /// </summary>
        /// <param name="name">name of the question</param>
        void displayQuestion(String name)
        {
            int cat = nameToCategory(name);
            int quest = nameToQuestion(name);
            question question = myGame.categories[cat].questions[quest];
            QuestionWindow qw = new QuestionWindow(question.display, question.correct);
            if (qw.ShowDialog() == true)
            {
                // answer was correct, add pont to the time
                scores[team - 1] += int.Parse(question.value);
                updateScores(); // update points display
            }
            stopClock();
        }

        /// <summary>
        /// Update the display of all scores
        /// </summary>
        private void updateScores()
        {
            t1score.Content = scores[0].ToString();
            t2score.Content = scores[1].ToString();
            t3score.Content = scores[2].ToString();
            t4score.Content = scores[3].ToString();
        }

        /// <summary>
        /// Get the category offset in the list from the question name
        /// </summary>
        /// <param name="name">question name</param>
        /// <returns>category offset in category list</returns>
        private int nameToCategory(String name)
        {
            return int.Parse(name.Substring(5)) - 1;
        }

        /// <summary>
        /// Get the question offset in the list from the question name
        /// </summary>
        /// <param name="name">question name</param>
        /// <returns>question offset in question list</returns>
        private int nameToQuestion(String name)
        {
            return int.Parse(name.Substring(1,1)) - 1;
        }

        /// <summary>
        /// restart the game with the same questions
        /// </summary>
        private void reset()
        {
            log.Info("restarting game");
            // Turn timer off
            if (timer != null)
            {
                timer.Stop();
                timeLeft = 0;
                timeLabel.Content = "30";
            }
            // Reset question list
            createButtonList();
            // Enable all questions
            enableQuestions();
            // Reset scores
            for (int i = 0; i < 4; i++)
                scores[i] = 0;
            t1score.Content = "0";
            t2score.Content = "0";
            t3score.Content = "0";
            t4score.Content = "0";
            // Reset team number
            team = 1;
            highlightTeam();
        }

        /// <summary>
        /// Reset the game to start over
        /// </summary>
        /// <param name="sender">(unused)</param>
        /// <param name="e">(unused)</param>
        private void resetButton_Click(object sender, RoutedEventArgs e)
        {
            reset();
        }

        /// <summary>
        /// Read a question file from the disk.  It assumes it is in the default
        /// directory for the program unless a path is given.
        /// </summary>
        /// <param name="fileName">file path</param>
        private void readFile(String fileName)
        {
            string appData = System.Environment.GetEnvironmentVariable("LOCALAPPDATA");
            // Try doing file operations on it.
            string dataDirectory = System.IO.Path.Combine(appData, "Jeopardy");
            string path = System.IO.Path.Combine(dataDirectory, fileName);
            log.InfoFormat("reading file {0}", path);
            // deserialize XML file
            using (StreamReader reader = new StreamReader(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(game));
                myGame = (game)serializer.Deserialize(reader);
                reader.Close();
            }
            // Make sure the format is okay
            verifyFormat();
            // Create category headings
            createHeadings();
        }

        /// <summary>
        /// Create category headings on game fro myGame category names
        /// </summary>
        private void createHeadings()
        {
            cat1.Content = myGame.categories[0].name;
            cat2.Content = myGame.categories[1].name;
            cat3.Content = myGame.categories[2].name;
            cat4.Content = myGame.categories[3].name;
            cat5.Content = myGame.categories[4].name;
            cat6.Content = myGame.categories[5].name;
        }

        /// <summary>
        /// Change to a new question file
        /// </summary>
        /// <param name="sender">ComboBox with item info</param>
        /// <param name="e">(unused_</param>
        private void fileButton_Click(object sender, RoutedEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            /*
             * I have seen the selected values returned as ComboBoxItems
             * or Strings depending on how I set them up.  This code
             * will handle either.
             */
            var value = box.SelectedValue;
            ComboBoxItem item = value as ComboBoxItem;
            string filename = "";
            if (item == null)
            {
                filename = value as String;
            }
            else
            {
                filename = (string)item.Content;
            }
            try
            {
                if(filename != null)
                    readFile(filename);
            }
            catch (Exception exc)
            {
                log.ErrorFormat("Error opening question file {0}", filename);
                MessageBox.Show("Error opening file: " + exc.Message);
            }
            // game has changed, so reset everything
            reset();
        }

        /// <summary>
        /// Perform basic integrity checks on the file format.
        /// </summary>
        private void verifyFormat()
        {
            // Check number of categories
            log.Info("verying question file format");
            if (myGame.categories.Count != 6)
            {
                log.ErrorFormat("Wrong category count: {0}", myGame.categories.Count);
                MessageBox.Show("Wrong category count");
                return;
            }
            foreach (category cat in myGame.categories)
            {
                // check number of questions
                if (cat.questions.Count != 5)
                {
                    log.ErrorFormat("Wrong question count {0} for category {1}",
                        cat.questions.Count, cat.name);
                    MessageBox.Show("Wrong question count for category " + cat.name);
                    return;
                }
                // Make sure there is a question value to match each row
                HashSet<String> vals = new HashSet<string>();
                foreach (question quest in cat.questions)
                {
                    vals.Add(quest.value);
                    if (quest.correct == null)
                    {
                        log.ErrorFormat("No correct answer for question with value {0}", quest.value);
                        MessageBox.Show("No correct answer for question with value " + quest.value);
                        return;
                    }
                    if (quest.display == null)
                    {
                        log.ErrorFormat("No displayed question for question with value {0}", quest.value);
                        MessageBox.Show("No displayed question for question with value " + quest.value);
                    }
                }
                String[] testValues = new string[]{"100","200","300","400","500"};
                foreach (String tVal in testValues)
                {
                    if (!vals.Contains(tVal))
                    {
                        log.ErrorFormat("No question with value {0} in category {1}", tVal, cat.name);
                        MessageBox.Show("No question for value " + tVal + " in category " + cat.name);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// For future use.  It is the same as select file combo box for now.
        /// </summary>
        /// <param name="sender">passed to combo box handler</param>
        /// <param name="e">passed to combo box handler</param>
        private void setupButton_Click(object sender, RoutedEventArgs e)
        {
            SetupWindow win = new SetupWindow();
            win.ShowDialog();
            // They may have changed the question bank, so update it
            questionBanks = Properties.Settings.Default.QuestionBanks;
            // Also, questions and category names may have changed, so update them as well
            fileButton.SelectedIndex = -1; // force changed file selection
            fileButton.SelectedIndex = 0;
            // Test integrity of questions
            log.InfoFormat("Returned from setup with questionBanks.Count = {0} and fileButton.SelectedIndex={1}",
                questionBanks.Count, fileButton.SelectedIndex);
            foreach (String text in questionBanks)
            {
                if (text == null)
                {
                    log.Warn("Question text is null");
                }
                else
                {
                    log.InfoFormat("Question text is {0}", text);
                }
            }
            if (fileButton.SelectedIndex > questionBanks.Count)
            {
                fileButton.SelectedIndex = 0;
            }
            fileButton.Items.Refresh();
        }

        private void mainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Save properties for next time
            log.Info("saving question bank in program settings");
            Properties.Settings.Default.QuestionBanks = questionBanks;
            Properties.Settings.Default.Save();
        }
    }
}
