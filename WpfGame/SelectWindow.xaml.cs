﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using log4net;

namespace Jeopardy
{
    /// <summary>
    /// Interaction logic for SelectWindow.xaml
    /// </summary>
    public partial class SelectWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SelectWindow));
        public SelectWindow()
        {
            InitializeComponent();
            fillQuestionBox();
        }

        void fillQuestionBox()
        {
            String folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            String path = System.IO.Path.Combine(folder, "Jeopardy");
            List<String> bankList = new List<string>();
            foreach (String filename in System.IO.Directory.EnumerateFiles(path, "*.xml"))
            {
                bankList.Add(System.IO.Path.GetFileName(filename));
                log.InfoFormat("Added {0} to ListBox", System.IO.Path.GetFileName(filename));
            }
            questionBox.DataContext = bankList;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            log.Info("OK button clicked");
            if (questionBox.SelectedItems.Count == 0)
            {
                log.Warn("No items selected");
                MessageBox.Show("You didn't select any question banks.  Ignoring request.",
                    "No Selection", MessageBoxButton.OK, MessageBoxImage.Warning);
                // Treat this as cancelled
                DialogResult = false;
                return;
            }
            // Add selected banks to the list
            Properties.Settings.Default.QuestionBanks.Clear();
            foreach (var item in questionBox.SelectedItems)
            {
                String name = item as String;
                Properties.Settings.Default.QuestionBanks.Add(name);
            }
            DialogResult = true;
        }
    }
}
