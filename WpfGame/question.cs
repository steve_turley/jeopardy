﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Jeopardy
{
    public class question
    {
        [XmlAttribute]
        public String value { get; set; }
        public String display { get; set; }
        public String correct { get; set; }
    }
}
