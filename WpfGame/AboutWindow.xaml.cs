﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Jeopardy
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        public string MyTitle { get; set; }
        public string Product { get; set; }
        public string Copyright { get; set; }
        public string Company { get; set; }
        public string Description { get; set; }
        public string MyVersion { get; set; }

        public AboutWindow()
        {
            InitializeComponent();
            GetInfo();
            DataContext = this;
        }

        private void GetInfo()
        {
            Assembly app = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute title =
                (AssemblyTitleAttribute)app.GetCustomAttributes(typeof(AssemblyTitleAttribute), false)[0];
            MyTitle = title.Title;
            AssemblyProductAttribute product =
                (AssemblyProductAttribute)app.GetCustomAttributes(typeof(AssemblyProductAttribute), false)[0];
            Product = product.Product;
            AssemblyCopyrightAttribute copyright =
                (AssemblyCopyrightAttribute)app.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false)[0];
            Copyright = copyright.Copyright;
            AssemblyCompanyAttribute company =
                (AssemblyCompanyAttribute)app.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false)[0];
            Company = company.Company;
            AssemblyDescriptionAttribute description =
                (AssemblyDescriptionAttribute)app.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false)[0];
            Description = description.Description;
            Version version = app.GetName().Version;
            MyVersion = version.ToString();
        }
    }
}