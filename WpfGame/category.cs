﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Jeopardy
{
    public class category
    {
        [XmlAttribute]
        public String name { get; set; }
        public List<question> questions { get; set; }
    }
}
