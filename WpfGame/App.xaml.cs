﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Deployment.Application;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using log4net;
using log4net.Config;
using log4net.Appender;

namespace Jeopardy
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(App));

        protected override void OnStartup(StartupEventArgs e)
        {
            // XmlConfigurator.Configure(new FileInfo("SmtcMail.xml"));

            XmlConfigurator.Configure();
            FixLogDirectory();
            log.Info("Starting Jeopardy application.");
            string appData = System.Environment.GetEnvironmentVariable("LOCALAPPDATA");
            // Try doing file operations on it.
            string newPath = Path.Combine(appData, "Jeopardy");
            try
            {
                if (!Directory.Exists(newPath))
                {
                    /*
                     * First time running with this user.
                     * Create directory and add example files
                     */
                    log.Info("Local Jeopardy directory doesn't exist");
                    log.InfoFormat("Creating directory {0}", newPath);
                    Directory.CreateDirectory(newPath);
                }
                String fname = Path.Combine(newPath, "bank1.xml");
                if (!File.Exists(fname))
                {
                    log.InfoFormat("Copying bank1.xml to {0}", fname);
                    File.Copy("bank1.xml", fname);
                }
                fname = Path.Combine(newPath, "bank2.xml");
                if (!File.Exists(fname))
                {
                    log.InfoFormat("Copying bank2.xml to {0}", fname);
                    File.Copy("bank2.xml", fname);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Error in directory operations: " + exc.Message);
                log.Error("Error in directory operations", exc);
            }
            base.OnStartup(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            log.Info("exiting program");
            base.OnExit(e);
        }

        private void FixLogDirectory()
        {
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                log.Info("application is network deployed");
                var dataDirectory = ApplicationDeployment.CurrentDeployment.DataDirectory;
                foreach (var appender in LogManager.GetRepository().GetAppenders())
                {
                    var fileAppender = appender as FileAppender;
                    if (fileAppender != null)
                    {
                        fileAppender.File = fileAppender.File.Replace("${LOCALAPPDATA}", dataDirectory);
                        fileAppender.ActivateOptions();
                        // LogWindow.LogFiles.Add(fileAppender.File); // for UI display of log file information
                    }
                }
            }
            else
            {
                log.Info("application is not network deployed");
                foreach (var appender in LogManager.GetRepository().GetAppenders())
                {
                    var fileAppender = appender as FileAppender;
                    if (fileAppender != null)
                    {
                        // LogWindow.LogFiles.Add(fileAppender.File); for UI display for log files
                    }
                }
            }
        }

        public App()
        {
            DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler(
                App_DispatcherUnhandledException);
        }

        private void App_DispatcherUnhandledException(object sender,
            DispatcherUnhandledExceptionEventArgs e)
        {
            log.Error("Unhandled exception at application level", e.Exception);
            MessageBoxResult result = MessageBox.Show(String.Format("Something went very wrong: {0}\n" +
                "Would you like to try and continue anyway?", e.Exception.Message), "Unhandled Error",
                MessageBoxButton.YesNo, MessageBoxImage.Error);
            if (result == MessageBoxResult.Yes)
            {
                log.Info("User chose to ignore error and continue.");
                e.Handled = true;
            }
            else
            {
                log.Info("User chose not to try and continue.");
                // throw(e.Exception);
                this.OnExit(null);
            }
        }
    }
}
